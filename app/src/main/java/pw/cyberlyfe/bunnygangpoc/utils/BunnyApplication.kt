package pw.cyberlyfe.bunnygangpoc.utils

import android.app.Application
import pw.cyberlyfe.bunnygangpoc.BuildConfig
import timber.log.Timber

class BunnyApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}