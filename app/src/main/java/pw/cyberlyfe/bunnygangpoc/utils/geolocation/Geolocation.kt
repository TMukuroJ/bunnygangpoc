package pw.cyberlyfe.bunnygangpoc.utils.geolocation

import android.content.Context
import android.location.Location
import android.os.Build
import android.util.Log
import com.google.android.gms.location.LocationServices
import org.json.JSONObject

class Geolocation {

    val TAG = "Geolocation"
    private lateinit var location: Location
    lateinit var locationJSON: JSONObject

    fun getLocationExample(context: Context){
        locationJSON = JSONObject()
        val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        try{
            val lastLoc = mFusedLocationClient.lastLocation
            lastLoc.addOnSuccessListener {
                if(it != null){
                    Log.d(TAG, "Done")
                    location = it
                    locationJSON.put("accuracy", location.accuracy)
                    locationJSON.put("altitude", location.altitude)
                    locationJSON.put("bearing", location.bearing)
                    if(Build.VERSION.SDK_INT >= 26){
                        locationJSON.put("bearingAccuracyDegrees", location.bearingAccuracyDegrees)
                        locationJSON.put("verticalAccuracyMeters", location.verticalAccuracyMeters)
                    }
                    locationJSON.put("isFromMockProvider", location.isFromMockProvider)
                    locationJSON.put("latitude", location.latitude)
                    locationJSON.put("longitude", location.longitude)
                    locationJSON.put("provider", location.provider)
                    locationJSON.put("speed", location.speed)
                    locationJSON.put("time", location.time)
                    Log.d(TAG, locationJSON.toString())
                }
            }
        } catch (e : SecurityException){
        }
    }

}