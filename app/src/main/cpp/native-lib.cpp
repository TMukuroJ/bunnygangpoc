#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_pw_cyberlyfe_bunnygangpoc_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from B++";
    return env->NewStringUTF(hello.c_str());
}
